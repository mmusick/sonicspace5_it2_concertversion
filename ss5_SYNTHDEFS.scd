


/** 2. LOAD THE SYNTH DEF'S */
(



(
SynthDef("SS2", {
	arg totalTime = 600, micGain = 1, micLag = 10, outGain = 1.0;

	var startTime, micLeft, micCenter, micRight, farMic1, farMic2, hasFreq, freq, hasFreqRight,
	freqRight, sig3DelayRatio, initFreq, pitchRatio, rhpfFreq, chainMicLeft,
	onsetsMicLeft, chainMicRight, onsetsMicRight, envMicLeftHit, micLeftAmplitude,
	micCenterAmplitude, micRightAmplitude, micLeftInEnv, sig1Normal,
	sig1DelayShort, sig1DelayA, sig1DelayB, sig2Chain, sig2PassBelow,
	sig2PassAbove, sig2High, sig2EnvSoft, sig2EnvMed, sig2EnvLoud, sig2Normal,
	sig2DelayShort, sig2DelayAA, sig2DelayAB, sig3Chain, sig3PassBelow,
	sig3PassAbove, sig3EnvSoft, sig3EnvMed, sig3Normal, sig3DelayShort,
	sig3DelayA, sig2DelayB, sig2DelayC, sigCenterDelay1, sigCenterDelay2, centerSig,
	backLeftSig, backRightSig, leftSig, rightSig, micEnv, micEnv1, totalMicEnv;

	// ~buf1 = Buffer.alloc(s, 512);			//allocate a buffer for the Onset Ugen
	// ~buf2 = Buffer.alloc(s, 512);			//allocate a buffer for the Onset Ugen
	// ~buf3 = Buffer.alloc(s, 1024, 1);		//allocate buffer for sig2 FFT chain
	// ~buf4 = Buffer.alloc(s, 1024, 1);		//allocate buffer for sig2 FFT chain
	~trig1 = Bus.control(s, 1);
	~trig2 = Bus.control(s, 1);

	startTime = Main.elapsedTime;		//startTime = SC's time since program opened.
	//(Used to get time since Synth Starts)

	micEnv = EnvGen.kr(
		Env.new([1, 0, 0, 1],
			[7, 1, 7]),
		gate: In.kr(~trig1));

	micEnv1 = EnvGen.kr(
		Env.new([1, 0, 0, 1],
			[15, 10, 10]),
		gate: In.kr(~trig2));

	/*		totalMicEnv = EnvGen.kr(
	Env.new([0, 0.3, 0.3, 0, 0.7, 0.7, 1, 1, 0.4, 0.4, 0],
	[totalTime*0.1, totalTime*0.1, 5, 10, totalTime*0.2,
	totalTime*0.1, totalTime*0.3, totalTime*0.05, totalTime*0.05, totalTime*0.1]),
	doneAction: 2
	);*/
	totalMicEnv = 1;

	//microphone inputs
	micLeft = SoundIn.ar(~mainL)*micEnv*micEnv1*totalMicEnv*micGain.lag(micLag);
	micCenter = SoundIn.ar(~canMic)*1.3*micEnv*micEnv1*totalMicEnv*micGain.lag(micLag);
	// micCenter = 0.1 * LPF.ar( Mix([ SoundIn.ar(8), SoundIn.ar(9) ])*1.3*micEnv*micEnv1*totalMicEnv*micGain.lag(micLag), 400 );
	micRight = SoundIn.ar(~mainR)*micEnv*micEnv1*totalMicEnv*micGain.lag(micLag);
	farMic1 = SoundIn.ar(~sideL)*micEnv*micEnv1*totalMicEnv*micGain.lag(micLag);
	farMic2 = SoundIn.ar(~sideR)*micEnv*micEnv1*totalMicEnv*micGain.lag(micLag);


	//frequency tracking tracking
	# freq, hasFreq = Pitch.kr(Mix.new([micCenter, micLeft]),
		initFreq: 325,
		ampThreshold: 0.003,
		peakThreshold: 0.1,
		median: 10);
	pitchRatio = LinExp.kr(((freq-80)/200), 0.1, 9, 0.2, 9);

	# freqRight, hasFreqRight = Pitch.kr(Mix.new([micCenter, micRight]),
		initFreq: 150,
		ampThreshold: 0.001,
		peakThreshold: 0.1,
		median: 1);
	sig3DelayRatio = LinLin.kr(freqRight, 80, 2500, 0.5, 4);
	rhpfFreq = LinLin.kr(freqRight, 80, 5000, 80, 500);


	//amplitude tracking
	micLeftAmplitude = Amplitude.ar(micLeft);
	micCenterAmplitude = Amplitude.ar(micCenter);
	micRightAmplitude = Amplitude.ar(micRight);


	//onset tracking
	chainMicLeft = FFT(LocalBuf(512), micLeft);
	onsetsMicLeft = Onsets.kr(chainMicLeft,
		1,								//threshold
		\rcomplex, 							//signal processing type
		relaxtime: 0, 						//time to relax from peak
		mingap: 2000, 						//min wait time in millisec
		medianspan: 2);						//size of window used (smoothing)
	SendTrig.kr(onsetsMicLeft, 1, {Main.elapsedTime});	//send trigger to client

	chainMicRight = FFT(LocalBuf(512), micRight);
	onsetsMicRight = Onsets.kr(chainMicRight,
		1.6,							//threshold
		\rcomplex, 							//signal processing type
		relaxtime: 0, 						//time to relax from peak
		mingap: 5, 							//min wait time in millisec
		medianspan: 7);						//size of window used (smoothing)
	SendTrig.kr(onsetsMicRight, 2, {Main.elapsedTime});	//send trigger to client


	//onset events
	~timeStamp = OSCresponder(s.addr,'/tr',{ arg time,responder,msg;	//when trigger perform task
		if(msg[2] == 1, {"Slow Down There Back Delay: ".post;	//post time when piezo is struck
			(time-startTime).postln;
			" ".postln;});									//task to perform
		// if(msg[2] == 2, {"pingDelay: ".post;	//post time when piezo is struck
		//					(time-startTime).postln;
		//					" ".postln;});
	}).add;

	envMicLeftHit = EnvGen.kr(
		Env.new([1, 6, 1],
			[60, 0.1],
			[4, -4]),
		onsetsMicLeft);
	micLeftInEnv = EnvGen.kr(Env.perc(0.03, 0.05, 0.05), onsetsMicRight);


	//sig1 Delay Line
	//introduces grains of sound into a ping pong effect
	//caused from micLeft peak onsets
	sig1Normal = Normalizer.ar(								//the signal
		//it is normalized on input
		farMic1*micLeftInEnv,		//input
		(1-micRightAmplitude)**2,	//outputLevel -> follows input level
		0.05);						//lookAheadTime
	sig1DelayShort = CombC.ar(								//plays one hit of the sound
		sig1Normal*0.8,
		6,
		0.2,
		0.8);
	sig1DelayA = CombC.ar(									//takes the single hit and repeats it
		sig1DelayShort,				//in
		2,							//maxDelayTime
		0.2,						//delayTime
		5);							//decayTime
	sig1DelayB = DelayC.ar(									//same but opposite channel and delayed by 0.1"
		CombC.ar(
			sig1DelayShort,				//in
			2,							//maxDelayTime
			0.2,						//delayTime
			5),							//decayTime
		0.1,
		0.1);


	//sig2 Delay Line
	sig2EnvSoft = EnvGen.ar(								//envelope for softer amplitude
		Env.adsr(0.4,					//attackTime
			0, 						//decayTime
			1, 						//sustainLevel as a portion of Peak
			0.2, 					//releaseTime
			0.9, 					//peakLevel
			4),						//curve of env
		InRange.ar(micLeftAmplitude, 	//a gate (if InRange then open)
			0.007, 				//min value
			0.0999));			//max value
	sig2EnvMed = EnvGen.ar(									//envelope for medium amplitude
		Env.adsr(0.6,					//attackTime
			0, 						//decayTime
			1, 						//sustainLevel as a portion of Peak
			0.1, 					//releaseTime
			0.9, 					//peakLevel
			4),						//curve of env
		InRange.ar(micLeftAmplitude, 	//a gate (if InRange then open)
			0.2, 				//min value
			0.499));
	sig2EnvLoud = EnvGen.ar(								//loud envelope amplitude
		Env.adsr(0.2,					//attackTime
			0, 						//decayTime
			1, 						//sustainLevel as a portion of Peak
			0.2, 					//releaseTime
			0.9, 					//peakLevel
			4),						//curve of env
		InRange.ar(micLeftAmplitude, 	//a gate (if InRange then open)
			0.5, 				//min value
			0.8));				//max value

	sig2Chain = FFT(LocalBuf(1024), farMic2);	//sig2 FFT chain
	sig2PassBelow = IFFT(
		PV_MagBelow(
			sig2Chain,
			0.5
	))*sig2EnvMed;


	sig2PassAbove = RHPF.ar(
		IFFT(
			PV_MagBelow(
				PV_MagAbove(
					sig2Chain,
					0.005),
				0.9)
		),
		freq*4,
		micRightAmplitude*2)*sig2EnvSoft;
	sig2High = IFFT(
		PV_BinScramble(
			sig2Chain,
			micRightAmplitude,
			(1-micLeftAmplitude));
	)*sig2EnvLoud;
	sig2Normal = Normalizer.ar(
		Mix.new([sig2PassBelow,
			sig2PassAbove,
			sig2High]),			//input
		(1-micLeftAmplitude)
		*(1-micCenterAmplitude),	//outputLevel -> follows input level
		0.05);												//lookAheadTime
	sig2DelayShort = CombC.ar(
		sig2Normal,
		6,
		1,
		2);
	sig2DelayAA = CombC.ar(
		sig2Normal,			//in
		60,					//maxDelayTime
		10*envMicLeftHit,	//delayTime
		//envLengthens Decay when Mic
		//recieves a peak trigger.
		15);			//decayTime
	sig2DelayAB = CombC.ar(
		sig2Normal,		//in
		60,				//maxDelayTime
		12*envMicLeftHit,	//delayTime
		//envLengthens Decay when Mic
		//recieves a peak trigger.
		15);



	sig3EnvSoft = EnvGen.ar(
		Env.adsr(1,			//attackTime
			0, 			//decayTime
			1, 			//sustainLevel as a portion of Peak
			0.2, 		//releaseTime
			0.5, 		//peakLevel
			4),			//curve of env
		InRange.ar(micCenterAmplitude, 	//a gate (if InRange then open)
			0.0081, 			//min value
			0.05));			//max value
	sig3EnvMed = EnvGen.ar(
		Env.adsr(0.2,			//attackTime
			0, 				//decayTime
			1, 				//sustainLevel as a portion of Peak
			0.05, 			//releaseTime
			0.9, 			//peakLevel
			4),				//curve of env
		InRange.ar(micCenterAmplitude, 	//a gate (if InRange then open)
			0.1, 			//min value
			0.499));


	//sig3 Delay Line
	sig3Chain = FFT(LocalBuf(1024), micCenter);
	sig3PassBelow = RHPF.ar(
		IFFT(
			PV_MagBelow(
				PV_MagAbove(
					sig3Chain,
					0.05),
				0.9)
		),
		rhpfFreq,
		(1-micLeftAmplitude))*0.6*sig3EnvSoft;
	sig3PassAbove = IFFT(
		PV_MagBelow(
			PV_MagAbove(
				sig3Chain,
				0.2),
			0.7),
		micLeftAmplitude)*sig3EnvMed;
	sig3Normal = Normalizer.ar(
		Mix.new([sig3PassBelow,
			sig3PassAbove]),		//input
		(1-micCenterAmplitude)*0.7,		//outputLevel -> follows input level
		0.05);

	sig3DelayShort = CombC.ar(
		sig3Normal,
		10,
		sig3DelayRatio.lag(4),
		1.2*sig3DelayRatio);
	sig3DelayA = CombC.ar(
		sig3Normal,				//in
		7,						//maxDelayTime
		6,						//delayTime
		//envLengthens Decay when Mic
		//recieves a peak trigger.
		7);

	//sig Main Back Delay Lines
	sig2DelayB = PitchShift.ar(
		CombL.ar(
			Mix.new([
				sig2DelayAA,	//in
				sig1DelayB,
				sig3DelayA]),
			16,					//maxDelayTime
			15,					//delayTime
			17),				//decayTime
		0.1,
		pitchRatio,
		0.1,
		0.01);
	sig2DelayC = PitchShift.ar(
		CombL.ar(
			Mix.new([
				sig2DelayAB,	//in
				sig1DelayA,
				sig3DelayA]),
			15,					//maxDelayTime
			13,					//delayTime
			17),				//decayTime
		0.1,
		pitchRatio,
		0.1,
		0.01);

	sigCenterDelay1 = CombL.ar(
		DelayL.ar(
			sig2DelayC,
			8,
			8),
		6,
		6,
		12)
	*micCenterAmplitude;
	sigCenterDelay2 = CombL.ar(
		DelayL.ar(
			sig2DelayB,
			8,
			8),
		6,
		6,
		12)
	*micCenterAmplitude;




	//Output assignment section
	~leftSig = Limiter.ar(
		Mix.new([sig2DelayAB*0.8,
			sig2DelayShort		//input
		]),
		0.9,				//desired level
		0.01)*outGain.lag(micLag);			//lookAheadTime
	~rightSig = Limiter.ar(
		Mix.new([
			sig2DelayAA*0.8,
			sig1DelayShort		//input
		]),
		0.9,				//desired level
		0.01)*outGain.lag(micLag);			//lookAheadTime
	~centerSigLeft = Limiter.ar(
		Mix.new([
			sig3DelayShort,
			sigCenterDelay1]),	//input
		0.9,				//desired level
		0.01)*outGain.lag(micLag);			//lookAheadTime
	~centerSigRight = Limiter.ar(
		Mix.new([
			sig3DelayShort,
			sigCenterDelay2]),	//input
		0.9,				//desired level
		0.01)*outGain.lag(micLag);			//lookAheadTime
	~backLeftSig = Limiter.ar(
		Mix.new([
			sig2DelayC,		//input
			sig1DelayA]),
		0.9,				//desired level
		0.01)*outGain.lag(micLag);			//lookAheadTime
	~backRightSig = Limiter.ar(
		Mix.new([
			sig2DelayB,		//input
			sig1DelayB]),
		0.9,				//desired level
		0.01)*outGain.lag(micLag);			//lookAheadTime

	x = (micLeftAmplitude+micRightAmplitude);
	y = Amplitude.kr(Mix.new([~leftSig, ~rightSig, ~backLeftSig, ~backRightSig, ~centerSigLeft, ~centerSigRight, micLeftAmplitude*0.3, micRightAmplitude*0.3, micCenterAmplitude*0.3]));




	Out.kr(~trig1, (y-0.4).lag(40));
	Out.kr(~trig2, (y-0.2).lag(15));

	//information
	// Poll.kr(Impulse.kr(6), micLeftAmplitude, \micLeftAmplitude);
	// Poll.kr(Impulse.kr(6), micCenterAmplitude, \micCenterAmplitude);
	// Poll.kr(Impulse.kr(6), micRightAmplitude, \micRightAmplitude);
	// Poll.kr(Impulse.kr(6), sig2EnvSoft, \sig2EnvSoft);
	// Poll.kr(Impulse.kr(6), sig3EnvSoft, \sig3EnvSoft);
	// Poll.kr(Impulse.kr(6), freq, \freq);
	// Poll.kr(Impulse.kr(6), pitchRatio, \pitchRatio);
	// Poll.kr(Impulse.kr(6), freqRight, \freqRight);
	// Poll.kr(Impulse.kr(6), sig3DelayRatio, \sig3DelayRatio);
	// Poll.kr(Impulse.kr(6), x, \addedMicAmp);
	// Poll.kr(Impulse.kr(6), x.lag(30), \addedMicAmpLag);
	// Poll.kr(Impulse.kr(6), y, \addedMicAmp);
	// Poll.kr(Impulse.kr(6), (y-0.25).lag(20), \shortMicAmpLag);
	// Poll.kr(Impulse.kr(6), micEnv, \micEnv);
	// Poll.kr(Impulse.kr(6), (y-0.4).lag(30), \endMicAmpLag);
	// Poll.kr(Impulse.kr(6), micEnv1, \micEnvEND);



	//		//Master Output Section
	Out.ar(32, ~leftSig);
	Out.ar(33, ~rightSig);
	Out.ar(36, ~backLeftSig);
	Out.ar(37, ~backRightSig);
	Out.ar(34, ~centerSigLeft);
	Out.ar(35, ~centerSigRight);
	// Out.ar(12, LPF.ar(			//LFE channel
	// 	Mix.new([~leftSig, ~rightSig, ~backLeftSig, ~backRightSig, ~centerSigLeft, ~centerSigRight])*1.0,
	// 100));

	//		Out.ar(0, ~leftSig);
	//		Out.ar(1, ~rightSig);
	//		Out.ar(2, HPF.ar(~backLeftSig, 80));
	//		Out.ar(3, HPF.ar(~backRightSig, 80));
	//		Out.ar(4, HPF.ar(~centerSig, 80));
	//		Out.ar(5, LPF.ar(			//LFE channel
	//					Mix.new([~backLeftSig, ~backRightSig, ~centerSig]),
	//					100));
	//		Out.ar(8, ~backLeftSig);
	//		Out.ar(9, ~backRightSig);
	//		Out.ar(6, ~centerSig);
	//		//Out.ar(9, SoundIn.ar(1));

	"SS2 Loaded".postln;
}).add;
);


SynthDef("RainDrops", {
	arg out = 2, in = 8, level = 0.6, delayTime = 1, cutOffFreq = 2000, qLevel = 0.4;
	var sig, env, outSig;

	// NUMBER OF CHANNELS -- CHANGE THIS AND RECOMPILE FOR SPECIFIC INSTALLATION
	var chan = 8;
	var width = IRand(1.0, 3.0);
	var pan = IRand(0.0, 2.0);

	"Rain Drops Keep Falling on My Head".postln;

	sig = SoundIn.ar(in)*8;
	//Delay the signal before going into the envelope
	sig = DelayC.ar( sig, maxdelaytime: 6, delaytime: delayTime );
	sig = HPF.ar( sig, 120 );
	sig = RLPF.ar( sig, freq: cutOffFreq, rq: qLevel );
	// Normalize the sound with an envelope
	env = Env.new( [0, 0, 1, 0.3, 0.3, 0.15, 0],
		[delayTime, 0.05, 0.05, 0.02, 0.02, 0.05],
		[4, -4, 0, 4, -4]);
	sig = Normalizer.ar( sig, level: EnvGen.kr( env, levelScale: level, doneAction: 2) );
	// Pan the signal
	outSig = PanAz.ar( numChans: chan, in: sig, pos: pan, width: width, orientation: 0 );

	// Output that sucker
	Out.ar( out, outSig );
}).add;



(
SynthDef( \howl1,
	{
		arg inputChan = 0, cntrlInputChan = 8, output = 10, thresh = 0.01, maxLifeTime = 60,
		micGain = 1.0, micLag = 10, gain = 1.0;
		var inSig, cntrlInSig, cntrlSig, gateCntrl, rhpfCntrl, pitchRaiseCntrl, pitch, hasFreq;
		var processedSig, outSig, env, envgen, maxLifeTimeEnv, rms, delay, invGain;

		"Hoooooowwwwwwwllllllll!!!!!".postln;



		inSig = SoundIn.ar(inputChan)*micGain.lag(micLag);
		cntrlInSig = SoundIn.ar(cntrlInputChan)*micGain.lag(micLag);


		// BandPass
		cntrlSig = BPF.ar( cntrlInSig, freq: 300, rq: 0.9 );
		cntrlSig = Amplitude.kr( cntrlSig ).lag(0.1, 6);
		gateCntrl = InRange.kr(cntrlSig, lo: thresh, hi: thresh+0.1);
		// cntrlSig.poll( label: \cntrlSig);

		env = Env.new( [ 0, 1, 1, 0 ], [ 0.1, 1, 2 ], releaseNode: 2 );
		envgen = EnvGen.kr( env, gate: gateCntrl );
		// envgen.poll( label: \mainEnv);

		rhpfCntrl = EnvGen.kr( Env.new([4000, 8000, 4000], [0.1, 6], curve: \exp), gate: gateCntrl);
		// rhpfCntrl.poll( label: \rhpfCntrl);

		pitchRaiseCntrl = EnvGen.kr( Env.new([1.0, 2.0, 1.0], [0.1, 4], curve: \exp), gate: gateCntrl);
		// pitchRaiseCntrl.poll( label: \pitchRais);

		processedSig = inSig  * envgen;
		processedSig = RHPF.ar( processedSig, freq: rhpfCntrl, rq: envgen.clip(0, 0.98) );
		processedSig = PitchShift.ar( processedSig, windowSize: 0.05, pitchRatio: pitchRaiseCntrl );
		processedSig = PitchShift.ar( processedSig, windowSize: 0.3, pitchRatio: 0.25 );
		processedSig = CompanderD.ar( processedSig, thresh: 0.1, slopeBelow: 0.7, slopeAbove: 0.25);
		processedSig = Normalizer.ar( processedSig, envgen )*0.7;

		rms = RunningSum.rms( processedSig, 100 ).lag(10)/*.poll*/;
		delay = LinLin.kr( rms, 0, 0.3, 1, 8 ).clip(0, 8).lag(10)/*.poll*/;
		invGain = LinLin.kr( rms, 0, 0.4, 1, 0 ).clip(0, 1)/*.poll*/;


		processedSig = CombL.ar( processedSig * invGain, 8, delay, 4 );




		maxLifeTimeEnv = EnvGen.kr( Env.new([1, 1, 0], [maxLifeTime, 4]), doneAction: 2);

		outSig = 0.7 * Limiter.ar( processedSig, 0.7 ) * gain.lag(micLag) * maxLifeTimeEnv;

		DetectSilence.ar( in: outSig, time: 100, doneAction: 2 );

		Out.ar( output, outSig );

	};
).add;
);




SynthDef(\digitalInsect,
	{ | threshHigh = 0.021, threshLow = 0.02, stretch = 1, shift = 0,
		lag = 10, outChan = 8, inputChan = 9, maxLifeTime = 60, noEnergySurvivalTime = 30,
		micGain = 1.0, micLag = 30, gain = 1.0|

		var sig, sig2, fftBuf, fftSig, fftSig2, shift2, inputSig, env, trig , trigdSig ;
		var frequency, hasFreq, centroid, rms, rms2, amplitude;
		var delayTime, decayTime, done;

		"High Digital Insects".postln;



		// Get Audio Signal
		inputSig = SoundIn.ar( inputChan )*micGain.lag(micLag);

		// get frequency domain representation
		// fftBuf = Buffer.alloc( s, 512, 1 );
		fftSig = FFT( LocalBuf(512), inputSig );

		// Get spectral Centroid
		centroid = SpecCentroid.kr(fftSig);
		centroid = RunningSum.kr(centroid, 1000) / 1000;
		// centroid.poll( label: \centroid2 );
		// This controls the bin shift for the less processed grains
		shift2 = LinLin.kr( in: centroid, srclo: 200, srchi: 6000, dstlo: -8, dsthi: 8 );
		// shift2.poll( label: \shift2 );


		// Less processed sound
		// only let bins above this magnitude pass (this removes noise)
		fftSig2 = PV_MagAbove( fftSig, threshold: 0.01);
		fftSig2 = PV_BinShift( fftSig2, stretch: 1, shift: shift2, interp: 1 );
		sig2 = IFFT( fftSig2 );

		// only let bins above this magnitude pass
		fftSig = PV_MagAbove( fftSig, threshold: threshLow);
		// only let bins below this magnitude pass
		fftSig = PV_MagBelow( fftSig, threshold: threshHigh );
		// shift and stretch remaining bins
		fftSig = PV_BinShift( fftSig, stretch: stretch, shift: shift, interp: 1 );
		// Return to time domain
		sig  = IFFT( fftSig );
		sig = Normalizer.ar( in: sig, level: 0.4, dur: 0.05 );
		// Get a transient tracker on the fft signal
		trig = Onsets.kr( fftSig, threshold: 0.02, relaxtime: 0.005, floor: 0.01 );

		// Add a grain of sound for each transient
		env = Env( [0, 1, 1, 0.2, 0], [0.01, 0.015, 0.015, 0.01], curve: \sin );
		trigdSig = sig2 * EnvGen.kr( env, trig );
		trigdSig = Normalizer.ar( in: trigdSig, level: 0.2, dur: 0.05 );

		// Combine the signals
		sig = Mix.new( [ sig, trigdSig ] );
		// reject extreme frequecies
		sig = HPF.ar( sig, freq: 100 );
		sig = LPF.ar( sig, freq: 18000 );

		// Get DelayTime from rms of signal
		rms = A2K.kr( RunningSum.rms( sig, 44100 ) );
		// rms.poll( label: \rms );
		rms = Lag.kr(rms, 2);
		// rms.poll( label: \rmsLag);


		delayTime = LinLin.kr( rms, srclo: 0, srchi: 0.1, dstlo: 0, dsthi: 5 ).clip(0, 10);
		// delayTime.poll( label: \PreLag_delay );
		// delayTime = Lag.kr( delayTime, lagTime: lag);
		// delayTime.poll( label: \delay);

		// Delay signal
		sig = CombC.ar( in: sig, maxdelaytime: 10, delaytime: delayTime, decaytime: delayTime*(10-(delayTime+2)) );


		// Limit output for safety
		sig = Limiter.ar( sig*2, level: 0.9, dur: 0.01 )*gain.lag(micLag);

		// Add a fade in.
		// (There is a popping sound in the first moment of initialization I cannot track down)
		sig = 0.7 * sig  * EnvGen.kr(Env([0, 0, 1, 1, 0], [0.2, 0.5, maxLifeTime, 5]), doneAction: 2);

		// Free when finished
		DetectSilence.ar( sig, time: noEnergySurvivalTime, doneAction: 2 );

		// Send that bitch out into the real world!
		Out.ar( outChan, sig );

	}
).add;





SynthDef(\digitalInsectLow,
	{ | threshHigh = 0.03, threshLow = 0.02, stretch = 1, shift = 0, maxLevel = 0.9,
		lag = 10, outChan = 9, inputChan = 8, maxLifeTime = 60, noEnergySurvivalTime = 10,
		micGain = 1.0, micLag = 30, gain = 1.0|

		var sig, sig2, fftBuf, fftSig, fftSig2, shift2, inputSig, env, trig , trigdSig ;
		var frequency, hasFreq, centroid, rms, rms2, amplitude;
		var delayTime, decayTime, done;

		"Low Digital Insects".postln;



		// Get Audio Signal
		inputSig = SoundIn.ar( inputChan )*micGain.lag(micLag);

		// get frequency domain representation
		// fftBuf = Buffer.alloc( s, 512, 1 );
		fftSig = FFT( LocalBuf(512), inputSig );

		// Get spectral Centroid
		centroid = SpecCentroid.kr(fftSig);
		centroid = RunningSum.kr(centroid, 1000) / 1000;
		// centroid.poll( label: \centroid2 );
		// This controls the bin shift for the less processed grains
		shift2 = LinLin.kr( in: centroid, srclo: 200, srchi: 6000, dstlo: 0, dsthi: -8 );
		// shift2.poll( label: \shift2 );


		// Less processed sound
		// only let bins above this magnitude pass (this removes noise)
		fftSig2 = PV_MagAbove( fftSig, threshold: 0.01);
		fftSig2 = PV_BinShift( fftSig2, stretch: 1, shift: shift2, interp: 1 );
		sig2 = IFFT( fftSig2 );

		// only let bins above this magnitude pass
		fftSig = PV_MagAbove( fftSig, threshold: threshLow);
		// only let bins below this magnitude pass
		fftSig = PV_MagBelow( fftSig, threshold: threshHigh );
		// shift and stretch remaining bins
		fftSig = PV_BinShift( fftSig, stretch: stretch, shift: shift, interp: 1 );
		// Return to time domain
		sig  = IFFT( fftSig );
		sig = Normalizer.ar( in: sig, level: 0.9, dur: 0.05 );
		// Get a transient tracker on the fft signal
		trig = Onsets.kr( fftSig, threshold: 0.02, relaxtime: 0.005, floor: 0.01 );

		// Add a grain of sound for each transient
		env = Env( [0, 1, 1, 0.2, 0], [0.01, 0.015, 0.015, 0.01], curve: \sin );
		trigdSig = sig2 * EnvGen.kr( env, trig );
		trigdSig = Normalizer.ar( in: trigdSig, level: 0.8, dur: 0.05 );

		// Combine the signals
		sig = Mix.new( [ sig, trigdSig ] );

		// Get DelayTime from rms of signal
		rms = A2K.kr( RunningSum.rms( sig, 44100 ) );
		// rms.poll( label: \rms );
		rms = Lag.kr(rms, 2);
		// rms.poll( label: \rmsLag);
		delayTime = LinLin.kr( rms, srclo: 0, srchi: 0.1, dstlo: 6, dsthi: 0 ).clip(0, 6);
		// delayTime.poll( label: \PreLag_delay );
		// delayTime = Lag.kr( delayTime, lagTime: lag);
		// delayTime.poll( label: \delay);
		// Delay signal
		sig = CombC.ar( in: sig, maxdelaytime: 10, delaytime: delayTime, decaytime: delayTime*(10-(delayTime+2)) );

		// reject extreme frequecies
		sig = HPF.ar( sig, freq: 200 );
		sig = LPF.ar( sig, freq: 1000 );
		// normalize one more time
		sig = Normalizer.ar( sig, maxLevel, 0.1 );

		// Limit output for safety
		sig = Limiter.ar( sig * 2, level: 0.9, dur: 0.1 );



		// Add a fade in.
		// (There is a popping sound in the first moment of initialization I cannot track down)
		sig = 0.7 * sig * gain.lag(micLag) * EnvGen.kr(Env([0, 0, 1, 1, 0], [0.2, 0.5, maxLifeTime, 5]), doneAction: 2);

		// Free when finished
		DetectSilence.ar( sig, time: noEnergySurvivalTime, doneAction: 2 );

		// Send that bitch out into the real world!
		Out.ar( outChan, sig );

	}
).add;


)

